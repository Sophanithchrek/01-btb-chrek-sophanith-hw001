//
//  ViewController.swift
//  MyApp
//
//  Created by Mavin on 9/7/20.
//  Copyright © 2020 hrd. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var lblHistory: UILabel!
    @IBOutlet weak var lblResult: UILabel!
    @IBOutlet weak var lblShowSign: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    var num1: Double = 0
    var num2: Double = 0
    var result: Double = 0
    var strNumberValue: String = ""
    
    @IBAction func btnNumber(_ sender: UIButton) {
        if lblResult.text!.count < 10 {
            if lblResult.text == "0" && sender.currentTitle! == "0" {
                lblResult.text = "0"
            } else {
                strNumberValue += sender.currentTitle!
                lblResult.text = strNumberValue
            }
        } else {
            print("Maximum digit (10) reached")
        }
    }
    
    @IBAction func btnSign(_ sender: UIButton) {
        strNumberValue = "" // reset value to give change for num2 to get value from lblResult
        isDotAdded = false
        lblShowSign.text = sender.currentTitle!
        lblHistory.text = lblResult.text
        num1 = Double(lblResult.text!)!
    }
    
    @IBAction func btnEqual(_ sender: UIButton) {
        var value: Array<String>
        switch lblShowSign.text! {
        case "+":
            num2 = Double(lblResult.text!)!
            result = num1 + num2
            lblResult.text = String(result)
            
            value = String(result).components(separatedBy: ".")
            if Int(value[1])! > 0 {
                lblResult.text = String(result)
            } else {
                lblResult.text = value[0]
            }
        case "-":
            num2 = Double(lblResult.text!)!
            result = num1 - num2
            lblResult.text = String(result)
            
            value = String(result).components(separatedBy: ".")
            if Int(value[1])! > 0 {
                lblResult.text = String(result)
            } else {
                lblResult.text = value[0]
            }
        case "x":
            num2 = Double(lblResult.text!)!
            result = num1 * num2
            lblResult.text = String(result)
            
            value = String(result).components(separatedBy: ".")
            if Int(value[1])! > 0 {
                lblResult.text = String(result)
            } else {
                lblResult.text = value[0]
            }
        case "÷":
            num2 = Double(lblResult.text!)!
            
            if num2 == 0 {
                lblResult.text = "Not a number"
            } else {
                result = num1 / num2
                lblResult.text = String(result)
                
                value = String(result).components(separatedBy: ".")
                if Int(value[1])! > 0 {
                    lblResult.text = String(result)
                } else {
                    lblResult.text = value[0]
                }
            }
        case "%":
            num2 = Double(lblResult.text!)!
            result = Double(Int(num1) % Int(num2))
            lblResult.text = String(result)
            
            value = String(result).components(separatedBy: ".")
            if Int(value[1])! > 0 {
                lblResult.text = String(result)
            } else {
                lblResult.text = value[0]
            }
        default:
            print("It is default")
        }
    }
    
    var isDotAdded: Bool = false
    @IBAction func btnDot(_ sender: UIButton) {
        if isDotAdded == false {
            isDotAdded = true
            
            if lblResult.text == "0" && sender.currentTitle! == "." {
                strNumberValue = "" // set temporary strNumberValue to prevent add double 00
                strNumberValue += "0"+sender.currentTitle!
                lblResult.text = strNumberValue
            } else {
                strNumberValue += sender.currentTitle!
                lblResult.text = strNumberValue
            }
        }
    }
    
    @IBAction func btnSwitchSign(_ sender: UIButton) {
        var val: Double = Double(lblResult.text!)!
        val = val * -1
        lblResult.text = String(val)
    }
    
    @IBAction func btnClear(_ sender: UIButton) {
        lblResult.text = "0"
        lblHistory.text = "0"
        lblShowSign.text = ""
        strNumberValue = ""
        isDotAdded = false
    }
    
}

